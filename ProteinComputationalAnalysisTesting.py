#Import directives
from Sequence import Sequence
import FileIO
import ProteinAnalysisTools

def main():
    #Variable declarations
    rnaSequenceFileName = "rna_sequence.txt"
    rnaSequenceFileContents = ""
    rnaSequenceObject = Sequence()
    proteinSequenceFileName = "protein_sequence.txt"
    proteinSequenceFileContents = ""
    proteinSequenceObject = Sequence()

    ###RNA to PROTEIN###

    #Read in RNA sequence
    rnaSequenceFileContents = FileIO.readSequence(rnaSequenceFileName)
    rnaSequenceObject.setRNASequenceString(rnaSequenceFileContents)

    #Run protein computational analysis
    rnaSequenceObject = ProteinAnalysisTools.translateToProtein(rnaSequenceObject)

    #Print output
    print "RNA Sequence Attributes:"
    print "------------------------"
    print "Protein Sequence:",rnaSequenceObject.getProteinSequenceString()
    print "------------------------"
    print ""

    ###PROTEIN COMPUTATIONAL ANALYSIS###
    proteinSequenceFileContents = FileIO.readSequence(proteinSequenceFileName)
    proteinSequenceObject.setProteinSequenceString(proteinSequenceFileContents)

    proteinSequenceObject = ProteinAnalysisTools.calculateMonoIsotopicProteinMass(proteinSequenceObject)
    proteinSequenceObject = ProteinAnalysisTools.calculateInferredMRNASequences(proteinSequenceObject)

    print "------------------------"
    print "Computational Values:"
    print "------------------------"
    print "Mono-Isotopic Protein Mass:",proteinSequenceObject.getMonoIsotopicProteinMass()
    print "Inferred MRNA Sequences:",proteinSequenceObject.getNumberOfInferredMRNASequences()


main()