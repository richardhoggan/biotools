#Module Name: ProteinTools.py
#Developer: Rich Hoggan
#Creation Date: 12/08/2016
#Description: Handles all protein computational analsysis.

#Module wide variable declarations
rnaToProteinDictionary = {
    "UUU":"F", "UUC":"F", "UUA":"L", "UUG":"L", "UCU":"S", "UCC":"S", "UCA":"S", "UCG":"S",
    "UAU":"Y", "UAC":"Y", "UAA":"STOP", "UAG":"STOP", "UGU":"C", "UGC":"C", "UGA":"STOP",
    "UGG":"W", "CUU":"L", "CUC":"L", "CUA":"L", "CUG":"L", "CCU":"P", "CCC":"P", "CCA":"P",
    "CCG":"P", "CAU":"H", "CAC":"H", "CAA":"Q", "CAG":"Q", "CGU":"R", "CGC":"R", "CGA":"R",
    "CGG":"R", "AUU":"I", "AUC":"I", "AUA":"I", "AUG":"M", "ACU":"T", "ACC":"T", "ACA":"T",
    "ACG":"T", "AAU":"N", "AAC":"N", "AAA":"N", "AAC":"N", "AAA":"K", "AAG":"K", "AGU":"S",
    "AGC":"S", "AGA":"R", "AGG":"R", "GUU":"V", "GUC":"V", "GUA":"V", "GUG":"V", "GCU":"A",
    "GCC":"A", "GCA":"A", "GCG":"A", "GAU":"D", "GAC":"D", "GAA":"E", "GAG":"E", "GGU":"G",
    "GGC":"G", "GGA":"G", "GGG":"G"}
monoIsotopicMassDictionary = {"A": 71.03711, "C": 103.00919, "D": 115.02694, "E": 129.04259, "F": 147.06841, \
                       "G": 57.02146, "H": 137.05891, "I": 113.08406, "K": 128.09496, "L": 113.08406, \
                       "M": 131.04049, "N": 114.04293, "P": 97.05276, "Q": 128.05858, "R": 156.10111, \
                       "S": 87.03203, "T": 101.04768, "V": 99.06841, "W": 186.07931, "Y": 163.06333}

def generateProteinFrequencyMap():
    """
    Method Name: generateProteinFrequencyMap()
    Method Description: Generates a frequency map of the rnaToProtein data structure.
    :return frequencyMap:<protein> to <frequency of occurrence (codon level)>:
    """
    #Variable declarations
    frequencyMap = {}

    #Iterate through the rnaToProteinMap and generate frequencies of each protein
    for currentRNACodon in rnaToProteinDictionary:
        #Get currentProtein mapped to the given RNA codon
        currentProtein = rnaToProteinDictionary.get(currentRNACodon)

        if currentProtein not in frequencyMap:
            currentCount = 1
            frequencyMap[currentProtein] = currentCount
        else:
            currentCount = frequencyMap[currentProtein]
            currentCount += 1
            frequencyMap[currentProtein] = currentCount

    #Return frequencyMap on completion
    return frequencyMap

def translateToProtein(sequenceObject):
    """
    Method Name: translateToProtein()
    Method Description: Translates a given RNA sequence to protein sequence.
    :param sequenceObject:
    :return sequenceObject:
    """
    #Variable declarations
    translatedProteinString = ""
    currentCodon = ""
    startingSliceIndex = 0
    endingSliceIndex = 3 #Since codons are 3 units in size, start the end at 3

    #Before iterating through the RNA sequence, make sure the RNA sequence contains
    #enough nucleotides to have an equal number of 3-unit sized codons
    if len(sequenceObject.getRNASequenceString()) % 3 == 0:
        #Iterate through the RNA sequence and generate protein sequence
        while startingSliceIndex < len(sequenceObject.getRNASequenceString()):
            #Slice the currentCodon from the RNA sequence
            currentCodon = sequenceObject.getRNASequenceString()[startingSliceIndex:endingSliceIndex]

            #Determine if the current codon is in the rnaToProteinMap
            if currentCodon in rnaToProteinDictionary:
                currentProtein = rnaToProteinDictionary.get(currentCodon)
                translatedProteinString += currentProtein

            #Increment counters to slice the next codon
            startingSliceIndex = endingSliceIndex
            endingSliceIndex += 3

    #Generate return object
    sequenceObject.setProteinSequenceString(translatedProteinString)
    return sequenceObject

def calculateMonoIsotopicProteinMass(sequenceObject):
    """
    Method Name: calculateMonoIsotopicProteinMass()
    Method Description: Calculates the mono-isotopic protein mass for the given protein sequence.
    :param sequenceObject:
    :return sequenceObject:
    """
    #Variable declarations
    currentProteinMass = 0.0
    monoIsotopicProteinMass = 0.0

    #Iterate through the protein sequence and calculate the mono-isotopic protein mass
    for currentProtein in sequenceObject.getProteinSequenceString():
        #Determine if the current protein is in the monoIsotopicMassDictionary
        if currentProtein in monoIsotopicMassDictionary:
            currentProteinMass = monoIsotopicMassDictionary.get(currentProtein)
            monoIsotopicProteinMass += currentProteinMass

    #Generate return object
    sequenceObject.setMonoIsotopicProteinMass(monoIsotopicProteinMass)
    return sequenceObject

def calculateInferredMRNASequences(sequenceObject):
    """
    Method Name: calculateInferredMRNASequences()
    Method Description: Calculates the inferred number of potential MRNA sequences from a given
    potein sequence.
    :param sequenceObject:
    :return sequenceObject:
    """
    #Variable declarations
    proteinFrequencyMap = {}
    numberOfInferredMRNASequences = 0

    #Generate the a frequency map of all protein to mrna instances
    proteinFrequencyMap = generateProteinFrequencyMap()

    #Account for stop codons in computation
    numberOfInferredMRNASequences = proteinFrequencyMap.get("STOP")

    #Iterate through the protein sequence
    for currentProtein in sequenceObject.getProteinSequenceString():
        #Determine if the current protein exists in the mono-isotopic protein dictionary
        if currentProtein in proteinFrequencyMap:
            currentFrequency = proteinFrequencyMap.get(currentProtein)
            numberOfInferredMRNASequences = (numberOfInferredMRNASequences * currentFrequency) % 1000000

    #Generate return object
    sequenceObject.setNumberOfInferredMRNASequences(numberOfInferredMRNASequences)

    return sequenceObject