#Module Name: DNATools.py
#Module Developer: Rich Hoggan
#Creation Date: 11/26/2016
#Description: Contains various tools for conducting DNA computational analysis.

def calculateNucleotideInstances(dnaSequenceObject):
    """
    Method Name: calculateNucleotideInstances()
    Method Description: Calculates the total instances of adenine, thymine, guanine
    and cytocine nucleotides within the given DNA sequence.
    :param dnaSequenceObject:
    :return dnaSequenceObject:
    """
    #Variable declarations
    adenineNucleotides = 0
    thymineNucleotides = 0
    guanineNucleotides = 0
    cytocineNucleotides = 0
    totalBasePairs = 0

    #Iterate through the DNA sequence and count nucleotide totals
    for currentNucleotide in dnaSequenceObject.getDNASequenceString():
        #Look for adenine, thymine, guanine, or cytocine nucleotides
        if currentNucleotide == "A":
            adenineNucleotides += 1
        elif currentNucleotide == "T":
            thymineNucleotides += 1
        elif currentNucleotide == "C":
            cytocineNucleotides += 1
        elif currentNucleotide == "G":
            guanineNucleotides += 1

        #Increment totalBasePairs counter
        totalBasePairs += 1

    #Generate return object
    dnaSequenceObject.setAdenineNucleotides(adenineNucleotides)
    dnaSequenceObject.setThymineNucleotides(thymineNucleotides)
    dnaSequenceObject.setGuanineNucleotides(guanineNucleotides)
    dnaSequenceObject.setCytocineNucleotides(cytocineNucleotides)
    dnaSequenceObject.setTotalBasePairs(totalBasePairs)

    return dnaSequenceObject

def calculateNucleotideRF(dnaSequenceObject):
    """
    Method Name: calculateNucleotideRF()
    Method Description: Calculates the relative frequency for each of the nucleotide types in
    a given DNA sequence.
    :param dnaSequenceObject:
    :return dnaSequenceObject:
    """
    #Variable declarations
    adenineNucleotideRF = 0.0
    thymineNucleotideRF = 0.0
    guanineNucleotideRF = 0.0
    cytocineNucleotideRF = 0.0

    #Calculate the relative frequency for each of the nucleotide types
    adenineNucleotideRF = (float(dnaSequenceObject.getAdenineNucleotides()) / dnaSequenceObject.getTotalBasePairs()) * 100
    thymineNucleotideRF = (float(dnaSequenceObject.getThymineNucleotides()) / dnaSequenceObject.getTotalBasePairs()) * 100
    guanineNucleotideRF = (float(dnaSequenceObject.getGuanineNucleotides()) / dnaSequenceObject.getTotalBasePairs()) * 100
    cytocineNucleotideRF = (float(dnaSequenceObject.getCytocineNucleotides()) / dnaSequenceObject.getTotalBasePairs()) * 100

    print "Guanine Nucleotide RF:",guanineNucleotideRF

    #Generate return object
    dnaSequenceObject.setAdenineNucleotideRF(adenineNucleotideRF)
    dnaSequenceObject.setThymineNucleotideRF(thymineNucleotideRF)
    dnaSequenceObject.setGuanineNucleotideRF(guanineNucleotideRF)
    dnaSequenceObject.setCytocineNucleotideRF(cytocineNucleotideRF)

    return dnaSequenceObject

def calculateGCContentRF(dnaSequenceObject):
    """
    Method Name: calculateGCContentRF()
    Method Description: Calculates the G/C content relative frequency for the given DNA sequence.
    :param dnaSequenceObject:
    :return dnaSequenceObject:
    """
    #Variable declarations
    gcContentRF = 0.0
    intermediateGCCalculation = 0.0

    #Calculate the gcContentRF using the following:
    #Sum guanine and cytocine nucleotides
    #Divide intermediateGCCalculation by the total base pairs to get GC content value
    intermediateGCCalculation = dnaSequenceObject.getGuanineNucleotides() + \
        dnaSequenceObject.getCytocineNucleotides()
    gcContentRF = (float(intermediateGCCalculation) / dnaSequenceObject.getTotalBasePairs()) * 100

    #Generate return object
    dnaSequenceObject.setGCContentRF(gcContentRF)

    return dnaSequenceObject

def generateReverseDNAComplement(dnaSequenceObject):
    """
    Method Name: generateReverseDNAComplement()
    Method Description: Iterates through a reversed version of the DNA sequence and
    generates the complement of the given DNA Sequence.
    Note the following changes:
    * Adenine -> Thymine
    * Thymine -> Adenine
    * Guanine -> Cytocine
    * Cytocine -> Guanine
    :param dnaSequenceObject:
    :return dnaSequenceObject:
    """
    #Variable declarations
    reversedDNAComplementSequence = ""

    #Iterate through the DNA sequence and invert the base pairs to generate
    #the appropriate complement sequence
    for currentNucleotide in reversed(dnaSequenceObject.getDNASequenceString()):
        if currentNucleotide == "A":
            reversedDNAComplementSequence += "T"
        elif currentNucleotide == "T":
            reversedDNAComplementSequence += "A"
        elif currentNucleotide == "G":
            reversedDNAComplementSequence += "C"
        elif currentNucleotide == "C":
            reversedDNAComplementSequence += "G"

    #Generate return object
    dnaSequenceObject.setReverseDNAComplementSequenceString(reversedDNAComplementSequence)

    return dnaSequenceObject

def translateToRNA(dnaSequenceObject):
    """
    Method Name: translateToRNA()
    Method Description: Conducts RNA traslation of the given DNA sequence.
    Note the following changes:
    * Thymine -> Uracil
    :param dnaSequenceObject:
    :return dnaSequenceObject:
    """
    #Variable declarations
    rnaSequence = ""

    #Iterate through the DNA sequence and conduct RNA translation
    for currentNucleotide in dnaSequenceObject.getDNASequenceString():
        if currentNucleotide == "T":
            rnaSequence += "U"
        else:
            rnaSequence += currentNucleotide

    #Generate return object
    dnaSequenceObject.setRNASequenceString(rnaSequence)

    return dnaSequenceObject