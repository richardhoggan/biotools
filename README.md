##Project Description
BioTools is a collection of my solutions to the Rosalind Bioinformatics algorithmic problems.  

The project is developed using Python 2.7.

##Currently available tools include: 
###DNA Analysis:
* calculateNucleotideInstances()
* calculateNucleotideRF()
* calculateGCContentRF()
* generateReverseDNAComplement()
* translateToRNA()

###Protein Analysis:
* generateProteinFrequencyMap()
* translateToProtein()
* calculateMonoIsotopicProteinMass()
* calculateInferredMRNASequences()

###Other Computational Tools:
* calculateHammingDistance()