from Sequence import Sequence
import FileIO
import DNAAnalysisTools

def main():
    #Variable declarations
    dnaSequenceFileName = "dna_sequence.txt"
    dnaSequenceFileContents = ""
    dnaSequenceObject = Sequence()

    #Read in DNA sequence
    dnaSequenceFileContents = FileIO.readSequence(dnaSequenceFileName)
    dnaSequenceObject.setDNASequenceString(dnaSequenceFileContents)

    #Run DNA computational analysis
    dnaSequenceObject = DNAAnalysisTools.calculateNucleotideInstances(dnaSequenceObject)
    dnaSequenceObject = DNAAnalysisTools.calculateNucleotideRF(dnaSequenceObject)
    dnaSequenceObject = DNAAnalysisTools.calculateGCContentRF(dnaSequenceObject)

    #Sequence generation
    dnaSequenceObject = DNAAnalysisTools.generateReverseDNAComplement(dnaSequenceObject)
    dnaSequenceObject = DNAAnalysisTools.translateToRNA(dnaSequenceObject)

    #Print output
    print "DNA Sequence Attributes:"
    print "------------------------"
    print "Nucleotide Instances:"
    print "------------------------"
    print "Adenine Nucleotides:",dnaSequenceObject.getAdenineNucleotides()
    print "Thymine Nucleotides:",dnaSequenceObject.getThymineNucleotides()
    print "Guanine Nucleotides:",dnaSequenceObject.getGuanineNucleotides()
    print "Cytocine Nucleotides:",dnaSequenceObject.getCytocineNucleotides()
    print ""
    print "------------------------"
    print "Nucleotide Relative Frequencies:"
    print "------------------------"
    print "Adenine RF:",dnaSequenceObject.getAdenineNucleotideRF()
    print "Thymine RF:", dnaSequenceObject.getThymineNucleotideRF()
    print "Guanine RF:", dnaSequenceObject.getGuanineNucleotideRF()
    print "Cytocine RF:", dnaSequenceObject.getCytocineNucleotideRF()
    print ""
    print "G/C Content RF:",dnaSequenceObject.getGCContentRF()
    print "------------------------"
    print "Sequence Generation:"
    print "------------------------"
    print "DNA Sequence:",dnaSequenceObject.getDNASequenceString()
    print "Reverse Compliment:",dnaSequenceObject.getReverseDNAComplimentSequenceString()
    print "RNA Translation:",dnaSequenceObject.getRNASequenceString()


main()