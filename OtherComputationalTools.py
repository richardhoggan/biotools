def calculateHammingDistance(dnaSequenceOneObject, dnaSequenceTwoObject):
    """
    Method Name: calculateHammingDistance()
    Method Description: Calculates the hamming distance between both sequences.
    Note the following example:
    A != T -> increment hamming distance
    A != A -> no action taken
    Also note that we assume the total base pairs in both sequences are equal.

    :param dnaSequenceOneObject:
    :param dnaSequenceTwoObject:
    :return hammingDistance:int:
    """
    #Variable declarations
    indexPointer = 0
    sequenceOneCurrentNucleotide = ""
    sequenceTwoCurrentNucleotide = ""
    hammingDistance = 0

    #Iterate through both nucleotides and calculate the hamming distance between them
    while indexPointer < dnaSequenceOneObject.getTotalBasePairs():
        #Get currentNucleotide at indexPointer for both DNA sequences
        sequenceOneCurrentNucleotide = dnaSequenceOneObject.getDNASequenceString()[indexPointer]
        sequenceTwoCurrentNucleotide = dnaSequenceTwoObject.getDNASequenceString()[indexPointer]

        #Determine if both nucleotides are non-matching
        #Non-matching indicates increment by one for hammingDistance
        if sequenceOneCurrentNucleotide != sequenceTwoCurrentNucleotide:
            hammingDistance += 1

        indexPointer += 1

    #Return hammingDistance on completion
    return hammingDistance