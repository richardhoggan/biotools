#Class: Sequence
#Developer: Rich Hoggan
#Creation Date: 12/12/2016
#Description: The data object class which handles all attributes associated with various
#sequence types as found in computational analysis of genomic sequences.
class Sequence:
    # Class Consructors
    def __init__(self):
        # Field definitions
        #File attribute fields
        self.fastaID = ""

        # DNA Sequence fields
        self.dnaSequenceString = ""
        self.rnaSequenceString = ""
        self.reverseComplementDNASequenceString = ""
        self.proteinSequenceString = ""

        # Nucleotide distribution
        self.adenineNucleotides = 0
        self.cytocineNucleotides = 0
        self.thymineNucleotides = 0
        self.guanineNucleotides = 0
        self.totalBasePairs = 0

        # Relative frequencies
        self.adenineNucleotideRF = 0.0
        self.cytocineNucleotideRF = 0.0
        self.thymineNucleotideRF = 0.0
        self.guanineNucleotideRF = 0.0
        self.gcContentRF = 0.0

        # Protein fields
        self.monoIsotopicProteinMass = 0.0

        #MRNA fields
        self.numberOfInferredMRNASequences = 0

    # Setters and getters
    #File attribute fields
    def setFASTAID(self, fastaID):
        self.fastaID = fastaID

    #Sequence fields
    def setDNASequenceString(self, dnaSequenceString):
        self.dnaSequenceString = dnaSequenceString

    def setRNASequenceString(self, rnaSequenceString):
        self.rnaSequenceString = rnaSequenceString

    def setReverseDNAComplementSequenceString(self, reverseComplementDNASequenceString):
        self.reverseComplementDNASequenceString = reverseComplementDNASequenceString

    def setProteinSequenceString(self, proteinSequence):
        self.proteinSequenceString = proteinSequence

    #Nucleotide fields
    def setAdenineNucleotides(self, adenineNucleotides):
        self.adenineNucleotides = adenineNucleotides

    def setCytocineNucleotides(self, cytocineNucleotides):
        self.cytocineNucleotides = cytocineNucleotides

    def setThymineNucleotides(self, thymineNucleotides):
        self.thymineNucleotides = thymineNucleotides

    def setGuanineNucleotides(self, guanineNucleotides):
        self.guanineNucleotides = guanineNucleotides

    def setTotalBasePairs(self, totalBasePairs):
        self.totalBasePairs = totalBasePairs

    #Relative frequency fields
    def setAdenineNucleotideRF(self, adenineNucleotideRF):
        self.adenineNucleotideRF = adenineNucleotideRF

    def setThymineNucleotideRF(self, thymineNucleotideRF):
        self.thymineNucleotideRF = thymineNucleotideRF

    def setGuanineNucleotideRF(self, guanineNucleotideRF):
        self.guanineNucleotideRF = guanineNucleotideRF

    def setCytocineNucleotideRF(self, cytocineNucleotideRF):
        self.cytocineNucleotideRF = cytocineNucleotideRF

    def setGCContentRF(self, gcContentRF):
        self.gcContentRF = gcContentRF

    #Protein fields
    def setMonoIsotopicProteinMass(self, monoIsotopicProteinMass):
        self.monoIsotopicProteinMass = monoIsotopicProteinMass

    #MRNA fields
    def setNumberOfInferredMRNASequences(self, numberOfInferredMRNAsequenes):
        self.numberOfInferredMRNASequences = numberOfInferredMRNAsequenes

    #Getter methods
    #File attribute fields
    def getFASTAID(self):
        return self.fastaID

    #Sequence fields
    def getDNASequenceString(self):
        return self.dnaSequenceString

    def getReverseDNAComplimentSequenceString(self):
        return self.reverseComplementDNASequenceString

    def getRNASequenceString(self):
        return self.rnaSequenceString

    def getProteinSequenceString(self):
        return self.proteinSequenceString

    #Nucleotide fields
    def getAdenineNucleotides(self):
        return self.adenineNucleotides

    def getCytocineNucleotides(self):
        return self.cytocineNucleotides

    def getThymineNucleotides(self):
        return self.thymineNucleotides

    def getGuanineNucleotides(self):
        return self.guanineNucleotides

    def getTotalBasePairs(self):
        return self.totalBasePairs

    #Relative frequency fields
    def getAdenineNucleotideRF(self):
        return self.adenineNucleotideRF

    def getThymineNucleotideRF(self):
        return self.thymineNucleotideRF

    def getGuanineNucleotideRF(self):
        return self.guanineNucleotideRF

    def getCytocineNucleotideRF(self):
        return self.cytocineNucleotideRF

    def getGCContentRF(self):
        return self.gcContentRF

    #Protein fields
    def getMonoIsotopicProteinMass(self):
        return self.monoIsotopicProteinMass

    #MRNA fields
    def getNumberOfInferredMRNASequences(self):
        return self.numberOfInferredMRNASequences