from Sequence import Sequence

def readSequence(sequenceFileName):
    """
    Method Name: readSequence()
    Method Description: Reads in a sequence from a given file.
    :param sequenceFileName:string:
    :return dnaSequence:string:
    """
    #Variable declarations
    sequence = ""
    sequenceFileHandle = None

    #Iterate through the sequence file and generate dna sequence
    try:
        sequenceFileHandle = open(sequenceFileName, "r")
        for currentLine in sequenceFileHandle:
            currentLine = currentLine.strip()
            sequence = sequence + currentLine
    except IOError:
        print "Unable to continue.  The DNA sequence file was not readable."
        print "Please double check you have the correct file permissions before continuing."
        print "Reference File Name:", sequenceFileName

    # Return dnaSequence on completion
    return sequence

def readFASTAFile(sequenceFileName, sequenceType):
    """
    Method Name: readFASTAFile()
    Method Description: Reads in a FASTA file and generates a return object of type Sequence
    which contains the genomic sequence.
    **Note: The following sequence types are expected:
        * DNA
        * RNA
        * PRT
    :param sequenceFileName:String
    :param sequenceType:String
    :return returnObject:Sequence
    """
    #Variable declarations
    sequence = ""
    sequenceFileHandle = None
    returnObject = Sequence()

    #Iterate through the sequence and parse out the fasta file ID before
    #reading the genomic sequence data (i.e >Rosalind_6404)
    try:
        sequenceFileHandle = open(sequenceFileName, "r")
        for currentLine in sequenceFileHandle:
            #FOrmat the current line before continuing
            currentLine = currentLine.strip()

            #Get the first index position of the current line
            firstIndexPosition = currentLine[0]
            if firstIndexPosition == ">":
                returnObject.setFASTAID(currentLine)
            else:
                sequence += currentLine
    except IOError:
        print "Unable to continue.  The DNA sequence file was not readable."
        print "Please double check you have the correct file permissions before continuing."
        print "Reference File Name:", sequenceFileName

    #Generate return object
    if sequenceType == "DNA":
        returnObject.setDNASequenceString(sequence)
    elif sequenceType == "RNA":
        returnObject.setRNASequenceString(sequence)
    elif sequenceType == "PRT":
        returnObject.setProteinSequenceString(sequence)

    return returnObject